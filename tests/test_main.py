from src.main import main

def test_main() -> None:
    assert main() == {
        "status_code":200,
        "message":"success"
    }
